# Gitlab CI/CD hello world exercise

A simple [flask](https://flask.palletsprojects.com/en/1.1.x/) app that serves
a single "Hello World" page.

It depends on the python package [xmlsec](https://pythonhosted.org/xmlsec/index.html),
which in turn depends on the system library [XMLsec](https://www.aleksey.com/xmlsec/).
It is only chosen because it requires an additional system package.
There is no need to understand how it works.

The service is served by [gunicorn](https://gunicorn.org/).

The assignment is to create a Gitlab CI/CD pipeline which implements the
following:

- Builds an image (using cache)
- Hadolints the `Dockerfile`
- Runs unit tests (tests TBA)
- Releases the image on Docker Hub on git tag pushes
- Implements a DAG


## Test
Run tests with
```
docker-compose up -d --build
docker-compose exec hello pytest
```
