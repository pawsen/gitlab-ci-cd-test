## Very verbosely commented Dockerfile for hello.py
# 1. Base image for the instructions below
FROM python:3.9

# 2. Change working directory to /app
WORKDIR /app

# 3. Copy requirement files from local fs (context) into the container
COPY sys-requirements.txt requirements.txt ./

# 4. Update system package lists (Debian based base image)
RUN set -ex \
    && apt-get update \
    # 5. Install system dependencies from sys-requirements.txt we just copied in
    && apt-get install -y --no-install-recommends $(grep -vE "^\s*#" sys-requirements.txt  | tr "\n" " ") \
    # 6. Create user for running the app (avoid escalation to root on the host)
    && useradd hellouser \
    # 7. Install python dependencies
    && pip3 install -r requirements.txt \
    # 8. Clean up a bit after apt
    && apt-get clean && rm -rf /var/lib/apt/lists/*

# 9. Copy source files to the container
COPY hello .

COPY tests .

EXPOSE 5000

# 10. Switch to the non-root user (avoid escalation to root on the host)
USER hellouser

# 11. Specify default command (run the gunicorn web server)
# - workers = 2 (got timeouts when default=1 used, heartbeats blocking the process?)
# - bind to port 5000 (as required by compose file), all interfaces
# - disable-redirect-access-to-syslog = access logs in stdout instead of syslog
CMD ["gunicorn", "--workers", "2", "--bind", "0.0.0.0:5000", "--disable-redirect-access-to-syslog", "--access-logfile", "-", "wsgi:app"]
