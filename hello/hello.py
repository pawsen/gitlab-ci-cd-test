# SPDX-FileCopyrightText: 2019-2020 Magenta ApS
# SPDX-License-Identifier: MPL-2.0

from flask import Flask
import xmlsec
import psycopg2
import os

# We don't actually need anything from xmlsec, but we wan't to make sure it is
# installed.
manager = xmlsec.KeysManager()

app = Flask(__name__)


@app.route("/")
def hello():
    return "Hello, World!"


@app.route("/db")
def db():

    with psycopg2.connect(
        host=os.getenv("HELLO_DB_HOST"),
        user=os.getenv("HELLO_DB_USER"),
        password=os.getenv("HELLO_DB_PASS"),
    ) as conn:
        with conn.cursor() as cur:
            cur.execute(
                "CREATE TABLE IF NOT EXISTS test (id serial PRIMARY KEY, "
                "num integer, data varchar);"
            )
            cur.execute(
                "INSERT INTO test (num, data) VALUES (%s, %s)",
                (100, "abc'def")
            )
            cur.execute("SELECT * FROM test;")

            return "data: %s" % cur.fetchone()[1]


if __name__ == "__main__":
    app.run(host="0.0.0.0")
